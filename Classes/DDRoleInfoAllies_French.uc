//=============================================================================
// DDRoleInfoAllies_French.uc
//=============================================================================
// Basic info for all French Roles.
//=============================================================================
// Dulce Et Decorum WW1 Expansion for Rising Storm 2: Vietnam
// Authored by SgtJoe & DD Team
//=============================================================================

class DDRoleInfoAllies_French extends DDRoleInfo
	abstract;

DefaultProperties
{
	// SecondaryWeapons(0)=
	
	SquadLeaderItems(0)=class'ROItem_Binoculars'
	
	RoleRootClass=class'DDRoleInfoAllies_French'
}
