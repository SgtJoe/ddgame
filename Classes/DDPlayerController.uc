//=============================================================================
// DDPlayerController.uc
//=============================================================================
// Player Controller.
//=============================================================================
// Dulce Et Decorum WW1 Expansion for Rising Storm 2: Vietnam
// Authored by SgtJoe & DD Team
//=============================================================================

class DDPlayerController extends ROPlayerController
	config(Game_DD);

`ifndef(RELEASE)

// Third-person camera mode for Pawn debugging
exec function Camera(optional bool free = false)
{
	ClientMessage(self @ "has a" @ self.Pawn.class);
	ServerCamera(free);
}

reliable server function ServerCamera(bool free)
{
	if (free)
	{
		SetCameraMode('FreeCam');
	}
	else
	{
		SetCameraMode('ThirdPerson');
	}
}

// Make yourself bleed to test bandaging
simulated exec function BleedTest()
{
	ROPawn(self.Pawn).PlayerHitZones[5].ZoneBleedingStatus = ROBS_Slow;
	ROPawn(self.Pawn).PlayerHitZones[5].bBleeding = true;
	
	ROPawn(self.Pawn).BleedingInstigator = self;
	
	ROPawn(self.Pawn).LastTakeHitInfo.DamageType = class'RODamageType';
	
	ROPawn(self.Pawn).StartBleeding();
}

`endif

defaultproperties
{
	
}
