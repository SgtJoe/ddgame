//=============================================================================
// DDWep_Lebel_Attach.uc
//=============================================================================
// French Lebel Model 1886 Rifle (Attachment)
//=============================================================================
// Dulce Et Decorum WW1 Expansion for Rising Storm 2: Vietnam
// Authored by SgtJoe & DD Team
//=============================================================================

class DDWep_Lebel_Attach extends ROWeaponAttachment;

var name BayonetBoneName;

simulated function InstantUnSetSpecialFunction()
{
	super.InstantUnSetSpecialFunction();
	Mesh.HideBoneByName(BayonetBoneName, PBO_Term);
}

simulated function InstantSetSpecialFunction()
{
	super.InstantSetSpecialFunction();
	Mesh.UnHideBoneByName(BayonetBoneName);
}

simulated static function ProcessDroppedPickup(RODroppedPickup Pickup, bool bGoToAltState)
{
	if(bGoToAltState)
	{
		Pickup.SetSkelControlStateByName(default.SpecialFunctionControlName, 0.f);
	}
	else
	{
		Pickup.SetSkelControlStateByName(default.SpecialFunctionControlName, 1.f);
		Pickup.HideBone(default.BayonetBoneName);
	}
}

defaultproperties
{
	CarrySocketName=WeaponSling
	ThirdPersonHandsAnim=MN9130_Handpose
	IKProfileName=MN9130
	
	Begin Object Name=SkeletalMeshComponent0
		SkeletalMesh=SkeletalMesh'WP_VN_3rd_Master.Mesh.MN9130_3rd_Master'
		AnimSets(0)=AnimSet'WP_VN_3rd_Master.Anim.MN9130_3rd_anim'
		AnimTreeTemplate=AnimTree'WP_VN_3rd_Master.AnimTree.MN9130_3rd_Tree'
		Animations=NONE
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy_Bounds.MN9130_3rd_Bounds_Physics'
		CullDistance=5000
	End Object
	
	MuzzleFlashSocket=MuzzleFlashSocket
	MuzzleFlashPSCTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_3rdP_Rifles_round'
	MuzzleFlashDuration=0.33
	MuzzleFlashLightClass=class'ROGame.RORifleMuzzleFlashLight'
	
	WeaponClass=class'DDWep_Lebel'
	
	ShellEjectSocket=ShellEjectSocket
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_VC_MN9130'
	bNoShellEjectOnFire=true
	
	CHR_AnimSet=AnimSet'CHR_VN_Playeranim_Master.Weapons.CHR_MN9130'
	
	FireAnim=Shoot
	FireLastAnim=Shoot_Last
	IdleAnim=Idle
	IdleEmptyAnim=Idle_Empty
	
	SpecialFunctionControlName=Bayonet_MN
	SpecialFunctionAnims(0)=Bayonet_Attach
	SpecialFunctionAnims(1)=Bayonet_Detach
	CH_SpecialFunctionAnims(0)=CH_Bayonet_Attach
	CH_SpecialFunctionAnims(1)=CH_Bayonet_Detach
	Prone_SpecialFunctionAnims(0)=Prone_Bayonet_Attach
	Prone_SpecialFunctionAnims(1)=Prone_Bayonet_Detach
	
	WP_SpecialFunctionAnims(0)=Bayonet_Attach
	WP_SpecialFunctionAnims(1)=Bayonet_Detach
	WP_Prone_SpecialFunctionAnims(0)=Prone_Bayonet_Attach
	WP_Prone_SpecialFunctionAnims(1)=Prone_Bayonet_Detach
	
	BayonetBoneName=Bayonet
}
