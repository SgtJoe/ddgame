//=============================================================================
// DDPawnAllies.uc
//=============================================================================
// Basic information for all Allied Pawns.
//=============================================================================
// Dulce Et Decorum WW1 Expansion for Rising Storm 2: Vietnam
// Authored by SgtJoe & DD Team
//=============================================================================

class DDPawnAllies extends DDPawn
	abstract;

simulated event byte ScriptGetTeamNum()
{
	return `ALLIES_TEAM_INDEX;
}

DefaultProperties
{
	bSingleHandedSprinting=false
	
	Begin Object Name=ROPawnSkeletalMeshComponent
		AnimSets(11)=AnimSet'CHR_Playeranim_Master.Anim.CHR_Russian_Unique'
	End Object
}
