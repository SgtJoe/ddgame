//=============================================================================
// DDRoleInfoAllies_French_Rifleman.uc
//=============================================================================
// French Rifleman Role.
//=============================================================================
// Dulce Et Decorum WW1 Expansion for Rising Storm 2: Vietnam
// Authored by SgtJoe & DD Team
//=============================================================================

class DDRoleInfoAllies_French_Rifleman extends DDRoleInfoAllies_French;

DefaultProperties
{
	RoleType=RORIT_Rifleman
	ClassTier=1
	ClassIndex=`ROCI_RIFLEMAN
	
	PrimaryWeapons(0)=class'DDWep_Lebel'
	
	// OtherItems(0)=
	
	ClassIcon=Texture2D'VN_UI_Textures.menu.class_icon_grunt'
}
