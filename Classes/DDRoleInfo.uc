//=============================================================================
// DDRoleInfo.uc
//=============================================================================
// Basic info for all Roles.
//=============================================================================
// Dulce Et Decorum WW1 Expansion for Rising Storm 2: Vietnam
// Authored by SgtJoe & DD Team
//=============================================================================

class DDRoleInfo extends RORoleInfo
	abstract;

DefaultProperties
{
	NumPrimaryUnlimitedWeapons=0
	
	bAllowPistolsInRealism=false
	
	// only used in the main menu
	ClassIconLarge=none
}
