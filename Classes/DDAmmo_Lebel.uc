//=============================================================================
// DDAmmo_Lebel.uc
//=============================================================================
// French Lebel Model 1886 Rifle (Ammunition)
//=============================================================================
// Dulce Et Decorum WW1 Expansion for Rising Storm 2: Vietnam
// Authored by SgtJoe & DD Team
//=============================================================================

class DDAmmo_Lebel extends ROAmmunition
	abstract;

defaultproperties
{
	CompatibleWeaponClasses(0)=class'DDWep_Lebel'
	
	InitialAmount=8
	Weight=0.1235
	ClipsPerSlot=3
}
