//=============================================================================
// DDGameInfoTerritories.uc
//=============================================================================
// Territories Gamemode.
//=============================================================================
// Dulce Et Decorum WW1 Expansion for Rising Storm 2: Vietnam
// Authored by SgtJoe & DD Team
//=============================================================================

class DDGameInfoTerritories extends ROGameInfoTerritories
	config(Game_DD);

defaultproperties
{
	PlayerControllerClass=	class'DDPlayerController'
	AIControllerClass=		class'ROAIController'
	HUDType=				class'ROHUD'
	TeamInfoClass=			class'ROTeamInfo'
	PawnHandlerClass=		class'ROPawnHandler'
	
	NorthRoleContentClasses=(LevelContentClasses=("DDGame.DDPawnAxisGermany"))
	SouthRoleContentClasses=(LevelContentClasses=("DDGame.DDPawnAlliesFrance"))
}
