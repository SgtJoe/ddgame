//=============================================================================
// DDPawnAxis.uc
//=============================================================================
// Basic information for all Axis Pawns.
//=============================================================================
// Dulce Et Decorum WW1 Expansion for Rising Storm 2: Vietnam
// Authored by SgtJoe & DD Team
//=============================================================================

class DDPawnAxis extends DDPawn
	abstract;

simulated event byte ScriptGetTeamNum()
{
	return `AXIS_TEAM_INDEX;
}

defaultproperties
{
	bSingleHandedSprinting=true
	
	Begin Object Name=ROPawnSkeletalMeshComponent
		AnimSets(11)=AnimSet'CHR_Playeranim_Master.Anim.CHR_German_Unique'
	End Object
}
