//=============================================================================
// DDLogger.uc
//=============================================================================
// Writes to the game's log file with a customizable header.
// Individual headers can be enabled/disabled in the config file.
//=============================================================================
// Dulce Et Decorum WW1 Expansion for Rising Storm 2: Vietnam
// Authored by SgtJoe & DD Team
//=============================================================================

class DDLogger extends Object
	config (Game_DD);

struct LogStruct
{
	var string LogType;
	var bool Enabled;
};

var config array<LogStruct> LogOptions;

static function DDLog(string Message, optional name Type)
{
	local int i, index;
	local bool LogExists;
	local LogStruct inData;
	
	Type = (Type == 'None') ? 'DD' : name('DD-'$Type);
	LogExists = false;
	index = 0;
	
	for (i = 0; i < default.LogOptions.length; i++)
	{
		if (string(Type) ~= default.LogOptions[i].LogType)
		{
			LogExists = true;
			index = i;
			break;
		}
	}
	
	if (LogExists && default.LogOptions[index].Enabled)
	{
		`log(Message,,Type);
	}
	else if (!LogExists)
	{
		inData.LogType = string(Type);
		inData.Enabled = true;
		
		default.LogOptions[default.LogOptions.length] = inData;
		
		StaticSaveConfig();
		
		`log("Creating new log" @ Type,,'DD');
		`log(Message,,Type);
	}
}