//=============================================================================
// DDMapInfo.uc
//=============================================================================
// Map-specific settings.
//=============================================================================
// Dulce Et Decorum WW1 Expansion for Rising Storm 2: Vietnam
// Authored by SgtJoe & DD Team
//=============================================================================

class DDMapInfo extends ROMapInfo;

function InitRolesForGametype(class<GameInfo> GameTypeClass, int MaxPlayers) {}

defaultproperties
{
	AxisReinforcementCount16=100
	AxisReinforcementCount32=200
	AxisReinforcementCount64=400
	
	AlliesReinforcementCount16=100
	AlliesReinforcementCount32=200
	AlliesReinforcementCount64=400
	
	AxisReinforcementDelay16=20
	AxisReinforcementDelay32=20
	AxisReinforcementDelay64=20
	
	AlliesReinforcementDelay16=20
	AlliesReinforcementDelay32=20
	AlliesReinforcementDelay64=20
	
	TimeLimit16=300
	TimeLimit32=600
	TimeLimit64=1200
	
	DefendingTeam=DT_Unspecified
	DefendingTeam16=DT_Unspecified
	DefendingTeam32=DT_Unspecified
	DefendingTeam64=DT_Unspecified
	
	NorthernRoles.Empty
	NorthernRoles(0)=(RoleInfoClass=class'RORoleInfoNorthernRifleman',Count=255)
	
	SouthernRoles.Empty
	SouthernRoles(0)=(RoleInfoClass=class'DDRoleInfoAllies_French_Rifleman',Count=255)
	
	ScoutReconInterval=300
	AerialReconInterval=300
	
	SouthernArtyStats(0)=(BatterySize=3,SalvoAmount=4,StrikeDelay=8,SalvoInterval=7.0,StrikePattern=1250,ShellClass=class'ROArtilleryShell')
	SouthernArtyStats(1)=(BatterySize=3,SalvoAmount=4,StrikeDelay=8,SalvoInterval=7.0,StrikePattern=1250,ShellClass=class'ROArtilleryShell')
	SouthernArtyStats(2)=(BatterySize=3,SalvoAmount=4,StrikeDelay=8,SalvoInterval=7.0,StrikePattern=1250,ShellClass=class'ROArtilleryShell')
	SouthernArtyStats(3)=(BatterySize=4,SalvoAmount=8,StrikeDelay=5,SalvoInterval=3.0,StrikePattern=1800,ShellClass=class'ROMortarShell')
	
	NorthernArtyStats(0)=(BatterySize=5,SalvoAmount=4,StrikeDelay=8,SalvoInterval=7.0,StrikePattern=1500,ShellClass=class'ROArtilleryShell')
	NorthernArtyStats(1)=(BatterySize=8,SalvoAmount=4,StrikeDelay=10,SalvoInterval=5.0,StrikePattern=2000,ShellClass=class'ROArtilleryShell')
}
