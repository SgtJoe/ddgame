//=============================================================================
// DDWep_Lebel_ActualContent.uc
//=============================================================================
// French Lebel Model 1886 Rifle (Content)
//=============================================================================
// Dulce Et Decorum WW1 Expansion for Rising Storm 2: Vietnam
// Authored by SgtJoe & DD Team
//=============================================================================

class DDWep_Lebel_ActualContent extends DDWep_Lebel;

DefaultProperties
{
	ArmsAnimSet=AnimSet'WP_DD_FR_Lebel.Anim.DDWP_Lebel_1st_Anims'
	
	Begin Object Name=FirstPersonMesh
		DepthPriorityGroup=SDPG_Foreground
		SkeletalMesh=SkeletalMesh'WP_DD_FR_Lebel.Mesh.DDWP_Lebel_AnimRef' // SkeletalMesh'WP_DD_FR_Lebel.Mesh.DDWP_Lebel_1st'
		PhysicsAsset=none
		AnimSets(0)=AnimSet'WP_DD_FR_Lebel.Anim.DDWP_Lebel_1st_Anims'
		AnimTreeTemplate=AnimTree'WP_DD_FR_Lebel.Anim.DDWP_Lebel_AnimTree'
		Scale=1.0
		FOV=70
	End Object
	
	Begin Object Name=PickupMesh
		SkeletalMesh=SkeletalMesh'WP_VN_3rd_Master.Mesh.MN9130_3rd_Master'
		PhysicsAsset=PhysicsAsset'WP_VN_3rd_Master.Phy.MN9130_3rd_Master_Physics'
		AnimTreeTemplate=AnimTree'WP_VN_3rd_Master.AnimTree.MN9130_3rd_Tree'
		CollideActors=true
		BlockActors=true
		BlockZeroExtent=true
		BlockNonZeroExtent=true//false
		BlockRigidBody=true
		bHasPhysicsAssetInstance=false
		bUpdateKinematicBonesFromAnimation=false
		PhysicsWeight=1.0
		RBChannel=RBCC_GameplayPhysics
		RBCollideWithChannels=(Default=TRUE,GameplayPhysics=TRUE,EffectPhysics=TRUE)
		bSkipAllUpdateWhenPhysicsAsleep=TRUE
		bSyncActorLocationToRootRigidBody=true
	End Object
	
	AttachmentClass=class'DDWep_Lebel_Attach'
}
