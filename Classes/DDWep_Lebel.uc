//=============================================================================
// DDWep_Lebel.uc
//=============================================================================
// French Lebel Model 1886 Rifle
//=============================================================================
// Dulce Et Decorum WW1 Expansion for Rising Storm 2: Vietnam
// Authored by SgtJoe & DD Team
//=============================================================================

class DDWep_Lebel extends ROProjectileWeapon
	abstract;

defaultproperties
{
	WeaponContentClass(0)="DDGame.DDWep_Lebel_ActualContent"
	
	RoleSelectionImage(0)=Texture2D'VN_UI_Textures.WeaponTex.VN_Weap_MN1891-30_Rifle'
	
	WeaponClassType=ROWCT_Rifle
	
	TeamIndex=`ALLIES_TEAM_INDEX
	
	Category=ROIC_Primary
	Weight=4.0 // kg
	
	InvIndex=`ROII_MN9130_Rifle
	
	InventoryWeight=0
	
	PlayerIronSightFOV=45.0//50
	
	PreFireTraceLength=2500 //50 Meters
	
	FiringStatesArray(0)=WeaponSingleFiring
	WeaponFireTypes(0)=EWFT_Custom
	WeaponProjectiles(0)=class'MN9130Bullet'
	FireInterval(0)=1.1//+1.5
	DelayedRecoilTime(0)=0.01
	Spread(0)=0.000414 // 1.5 MOA
	
	FiringStatesArray(ALTERNATE_FIREMODE)=WeaponManualSingleFiring
	WeaponFireTypes(ALTERNATE_FIREMODE)=EWFT_Custom
	WeaponProjectiles(ALTERNATE_FIREMODE)=class'MN9130Bullet'
	FireInterval(ALTERNATE_FIREMODE)=1.1//+1.5
	DelayedRecoilTime(ALTERNATE_FIREMODE)=0.01
	Spread(ALTERNATE_FIREMODE)=0.000414 // 1.5 MOA
	
	AltFireModeType=ROAFT_Bayonet
	
	MinBurstAmount=1
	MaxBurstAmount=2
	
	maxRecoilPitch=725//850
	minRecoilPitch=725
	maxRecoilYaw=300
	minRecoilYaw=-300
	minRecoilYawAbsolute=100
	RecoilRate=0.1
	RecoilMaxYawLimit=500
	RecoilMinYawLimit=65035
	RecoilMaxPitchLimit=2000
	RecoilMinPitchLimit=63535
	RecoilISMaxYawLimit=500
	RecoilISMinYawLimit=65035
	RecoilISMaxPitchLimit=1500
	RecoilISMinPitchLimit=64035
	RecoilBlendOutRatio=0.35//0.75//0.45
	RecoilViewRotationScale=0.75
	
	InstantHitDamage(0)=179
	InstantHitDamage(1)=179
	
	InstantHitDamageTypes(0)=class'RODmgType_MN9130Bullet'
	InstantHitDamageTypes(1)=class'RODmgType_MN9130Bullet'
	
	MuzzleFlashSocket=MuzzleFlashSocket
	MuzzleFlashPSCTemplate=ParticleSystem'FX_VN_Weapons.MuzzleFlashes.FX_VN_MuzzleFlash_1stP_Rifles_round'
	MuzzleFlashDuration=0.33
	MuzzleFlashLightClass=class'ROGame.RORifleMuzzleFlashLight'
	
	ShellEjectSocket=ShellEjectSocket
	ShellEjectPSCTemplate=ParticleSystem'FX_VN_Weapons.ShellEjects.FX_Wep_ShellEject_VC_MN9130'
	
	bHasIronSights=true
	bHasManualBolting=true
	bAmmoCheckDoesBolting=true
	
	WeaponPutDownAnim=Lebel1866_Holster
	WeaponEquipAnim=Lebel1866_Draw
	WeaponDownAnim=Lebel1866_Down
	WeaponUpAnim=Lebel1866_Up
	
	WeaponFireAnim(0)=Lebel1866_Fire_Hip
	WeaponFireAnim(1)=Lebel1866_Fire_Hip
	WeaponFireLastAnim=Lebel1866_Fire_Hip_Last
	
	WeaponFireShoulderedAnim(0)=Lebel1866_Bolt_Hip
	WeaponFireShoulderedAnim(1)=Lebel1866_Bolt_Hip
	WeaponFireLastShoulderedAnim=Lebel1866_Fire_Hip_Last
	
	WeaponFireSightedAnim(0)=Lebel1866_Fire_Iron
	WeaponFireSightedAnim(1)=Lebel1866_Fire_Iron
	WeaponFireLastSightedAnim=Lebel1866_Fire_Iron_Last
	
	WeaponManualBoltAnim=Lebel1866_Bolt_Hip
	
	WeaponIdleAnims(0)=Lebel1866_Idle
	WeaponIdleAnims(1)=Lebel1866_Idle
	
	WeaponIdleShoulderedAnims(0)=Lebel1866_Idle
	WeaponIdleShoulderedAnims(1)=Lebel1866_Idle
	
	WeaponIdleSightedAnims(0)=Lebel1866_Idle
	WeaponIdleSightedAnims(1)=Lebel1866_Idle
	
	WeaponCrawlStartAnim=Lebel1866_Crawl_In
	WeaponCrawlingAnims(0)=Lebel1866_Crawl_Loop
	WeaponCrawlEndAnim=Lebel1866_Crawl_Out
	
	WeaponReloadSingleBulletAnim=Lebel1866_Reload_Insert
	WeaponReloadEmptySingleBulletAnim=Lebel1866_Reload_Insert
	
	WeaponOpenBoltAnim=Lebel1866_Reload_Start
	WeaponOpenBoltEmptyAnim=Lebel1866_Reload_Start_Empty
	
	WeaponCloseBoltAnim=Lebel1866_Reload_End
	
	WeaponAmmoCheckAnim=Lebel1866_CheckAmmo
	
	WeaponSprintStartAnim=Lebel1866_Sprint_2Hand_In
	WeaponSprintLoopAnim=Lebel1866_Sprint_2Hand_Loop
	WeaponSprintEndAnim=Lebel1866_Sprint_2Hand_Out
	
	Weapon1HSprintStartAnim=Lebel1866_Sprint_1Hand_In
	Weapon1HSprintLoopAnim=Lebel1866_Sprint_1Hand_Loop
	Weapon1HSprintEndAnim=Lebel1866_Sprint_1Hand_Out
	
	WeaponMantleOverAnim=Lebel1866_Mantle
	
	WeaponSpotEnemyAnim=		Lebel1866_Spot_Hip
	WeaponSpotEnemySightedAnim=	Lebel1866_Spot_Irons
	
	WeaponMeleeAnims(0)=Lebel1866_Melee_Light
	WeaponMeleeHardAnim=Lebel1866_Melee_Strong
	
	WeaponBayonetMeleeAnims(0)=Lebel1866_Bayonet_Stab_Light
	WeaponBayonetMeleeHardAnim=Lebel1866_Bayonet_Stab_Strong
	
	MeleePullbackAnim=Lebel1866_PullBack
	MeleeHoldAnim=Lebel1866_PullBack_Hold
	
	WeaponAttachBayonetAnim=Lebel1866_Bayonet_On
	WeaponDetachBayonetAnim=Lebel1866_Bayonet_Off
	
	EquipTime=+0.66//1
	PutDownTime=+0.66//0.35
	
	bDebugWeapon=true
	
	// BoltControllerNames[0]=Hammer_9130
	
	BayonetSkelControlName=Bayonet_Lebel
	
	ISFocusDepth=46.5
	ISFocusBlendRadius=0.5
	
	MaxAmmoCount=8
	AmmoClass=class'DDAmmo_Lebel'
	bUsesMagazines=false
	InitialNumPrimaryMags=20//10
	bLosesRoundOnReload=true
	bPlusOneLoading=false
	bCanReloadNonEmptyMag=false
	bCanLoadStripperClip=false
	bCanLoadSingleBullet=true
	StripperClipBulletCount=5
	PenetrationDepth=17
	MaxPenetrationTests=3
	MaxNumPenetrations=2
	PerformReloadPct=0.90f
	
	ZoomInRotation=(Pitch=-910,Yaw=0,Roll=2910)
	
	PlayerViewOffset=(X=2.5,Y=4.5,Z=-1.25)
	ShoulderedPosition=(X=2.5,Y=4.5,Z=-1.25)
	
	IronSightPosition=(X=0,Y=0,Z=0)
	
	bUsesFreeAim=true
	
	ZoomInTime=0.25//0.35
	ZoomOutTime=0.25//0.22
	
	FreeAimMaxYawLimit=2000
	FreeAimMinYawLimit=63535
	FreeAimMaxPitchLimit=1500
	FreeAimMinPitchLimit=64035
	FreeAimISMaxYawLimit=500
	FreeAimISMinYawLimit=65035
	FreeAimISMaxPitchLimit=350
	FreeAimISMinPitchLimit=65185
	FullFreeAimISMaxYaw=250
	FullFreeAimISMinYaw=65285
	FullFreeAimISMaxPitch=175
	FullFreeAimISMinPitch=65360
	FreeAimHipfireOffsetX=45
	
	Begin Object Class=ForceFeedbackWaveform Name=ForceFeedbackWaveformShooting1
		Samples(0)=(LeftAmplitude=30,RightAmplitude=50,LeftFunction=WF_Constant,RightFunction=WF_Constant,Duration=0.200)
	End Object
	WeaponFireWaveForm=ForceFeedbackWaveformShooting1
	
	CollisionCheckLength=61.5
	
	FireCameraAnim[0]=CameraAnim'1stperson_Cameras.Anim.Camera_MN9130_Shoot'
	FireCameraAnim[1]=CameraAnim'1stperson_Cameras.Anim.Camera_MN9130_Shoot'
	
	SuppressionPower=15
	
	SwayScale=0.6
	
	bHasBayonet=true
	BayonetAttackRange=103.0
	MeleeAttackCoolDownInSeconds=0.4
	WeaponBayonetLength=21.6
	RecoilModBayonetAttached=0.95f
	
	WeaponFireSnd(DEFAULT_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_Mosin.Play_WEP_Mosin_Fire_Single_3P',FirstPersonCue=AkEvent'WW_WEP_Mosin.Play_WEP_Mosin_Fire_Single')
	WeaponFireSnd(ALTERNATE_FIREMODE)=(DefaultCue=AkEvent'WW_WEP_Mosin.Play_WEP_Mosin_Fire_Single_3P',FirstPersonCue=AkEvent'WW_WEP_Mosin.Play_WEP_Mosin_Fire_Single')
}
